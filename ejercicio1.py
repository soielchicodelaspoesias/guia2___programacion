
def cambiar(dic, nueva):
    #paso la formula molecular a una lista, y guado la key
    for i in dic:
        contador = 0
        lista = list(dic[i])
        texto = i
        #busco si hay una S
        for j in lista:
            if j == 'S':
                contador += 1
# busco las posiciones de las letras        
        c = lista.index('C')
        h = lista.index('H')
        n = lista.index('N')
        o = lista.index('O')
# creo listas de letra en letra y las junto
        lista1 = "".join([lista[i] for i in range(c, h)])
        lista2 = "".join([lista[i] for i in range(h, n)])
        lista3 = "".join([lista[i] for i in range(n, o)])
# las añado al nuevo diccionario
        nueva[lista1] = texto
        nueva[lista2] = texto
        nueva[lista3] = texto
# si hay una s 
        if contador == 1:
            s = lista.index('S')
            lista4 = "".join([lista[i] for i in range(o, s)])
            lista5 = "".join([lista[i] for i in range(s, len(lista))])
            nueva[lista4] = texto
            nueva[lista5] = texto
        else:
            lista4 = "".join([lista[i] for i in range(o, len(lista))])
            nueva[lista4] = texto
        print("Nuevo diccionario")
        print(nueva)

dic = {'Histidine': 'C6H9N3O2'}
print("diccioonario a modificar")
print(dic)
nueva = {}
cambiar(dic, nueva)