import json
amino = {'Histidine': 'C6H9N3O2',
         'Isoleucine': 'C6H13NO2',
         'Leucine': 'C6H13NO2',
         'Lysine': 'C6H14N2O2',
         'Methionine': 'C5H11NO2S',
         'Phenylalanine': 'C9H11NO2',
         'Threonine': 'C4H9NO3',
         'Tryptophan': 'C11H12N2O2',
         'Valine': 'C5H11NO2',
         'Alanine': 'C3H7NO2',
         'Glutamic': 'C5H9NO4',
         'Aspartic': 'C4H7NO4',
         'Asparagine': 'C4H8N2O3',
         'Arginine': 'C6H14N4O2',
         'Cysteine': 'C3H7NO2S',
         'Glutamine': 'C5H10N2O3',
         'Tyrosine': 'C9H11NO3',
         'Glycine': 'C2H5NO2',
         'Proline': 'C5H9NO2',
         'Serine': 'C3H7NO3'}


# Comprueba si el nombre ingresado pertenece a algun aminoacido
def comprobacion_name(amino, amino_insert):
    a = input("Ingrese aminoacido: ")
    j = 0
    key = ""
    for i in amino:
        if a == i:
            key = a
            j += 1
            comprobacion_formule(amino, key, amino_insert)
    if j == 0:
        print("El aminoacido que ingreso no existe")
        comprobacion_name(amino, amino_insert)


# Comprueba que la formula sea a misma que tiene el aminoacido en la base de datos
def comprobacion_formule(amino, key, amino_insert):
    a = input("Ingrese la formula molecular: ")
    if a == amino[key]:
        amino_insert[key] = a
        save_json(amino_insert)
        print("aminoacidos insertados: ")
        imprimir_json()
        print("Que desea hacer ahora")
        menu(amino, amino_insert)
    else:
        print("Formula molecular incorrecta")
        comprobacion_formule(amino, key, amino_insert)


# guarda los cambios en el archivo json
def save_json(amino_insert):
    with open('amino_insert.json', 'w') as fichero:
        json.dump(amino_insert, fichero, indent=4)


# imprimir el archivo json
def imprimir_json():
    archivo = open("amino_insert.json")
    for i in archivo:
        print(i)
    archivo.close()


# eliminar aminoacido
def eliminar_amino(amino, amino_insert):
    if len(amino_insert) == 0:
        print("No hay existe ningun aminoacido en la base de datos")
        print("Que desea hacer: ")
        menu(amino, amino_insert)
    else:
        a = input("Ingrese aminoacido que desea eliminar: ")
        k = amino_insert.copy()
        for i in k:
            if i == a:
                del amino_insert[i]
                save_json(amino_insert)
                print("Aminoacido eliminado")
                imprimir_json()
                print("Que desea hacer ahora: ")
                menu(amino, amino_insert)

            else:
                print("El aminoacido que ingreso no esta en la base de datos")
                eliminar_amino(amino_insert)


# buscar aminoados, primero se corta por atomo, aunque hay un error no se como hacer que por una key me de dos valores
def buscar_amino(amino_insert):
    lista = []
    nueva = {}
    contador = 0
    contador2 = 0
    for i in amino_insert:
        lista = list(amino_insert[i])
        texto = i
        for j in lista:
            if j == 'S':
                contador += 1
        c = lista.index('C')
        h = lista.index('H')
        n = lista.index('N')
        o = lista.index('O')
        lista1 = "".join([lista[i] for i in range(c, h)])
        lista2 = "".join([lista[i] for i in range(h, n)])
        lista3 = "".join([lista[i] for i in range(n, o)])
        nueva[lista1] = texto
        nueva[lista2] = texto
        nueva[lista3] = texto
        if contador == 1:
            s = lista.index('S')
            lista4 = "".join([lista[i] for i in range(o, s)])
            lista5 = "".join([lista[i] for i in range(s, len(lista))])
            nueva[lista4] = texto
            nueva[lista5] = texto
        else:
            lista4 = "".join([lista[i] for i in range(o, len(lista))])
            nueva[lista4] = texto
    b = input("Ingrese el atomo que desae buscar")
    print(nueva[b])


# editar aminoacidos
def editar_amino(amino, amino_insert):
    print("1 - Editar nombre")
    print("2 - Editar formula molecular")
    a = int(input("Inserte una opcion: "))
    if a == 1:
        old = input("Inserte el nombre del aminoacido que desea editar: ")
        new = input("Inserte el nuevo nombre del aminoacido: ")
        amino_insert[new] = amino_insert.pop(old)
        save_json(amino_insert)
        print("aminoacido editado: ")
        imprimir_json()
        print("Que desea hacer ahora")
        menu(amino, amino_insert)
    if a == 2:
        old = input("Inserte el nombre del aminoacido que desea editar: ")
        new = input("Ingrese su nueva formula molecular: ")
        amino_insert[old] = new
        save_json(amino_insert)
        print("aminoacido editado: ")
        imprimir_json()
        print("Que desea hacer ahora")
        menu(amino, amino_insert)






def menu(amino, amino_insert):
    print("1 - Ingresar aminoacido")
    print("2 - Buscar amoniacidos por atomos")
    print("3 - Eliminar aminoacido")
    print("4 - Editar aminoacido")
    print("5 - Salir")
    opcion = int(input("Ingrese la opcion que va a ingresar:"))
    if opcion == 1:
        comprobacion_name(amino, amino_insert)
    if opcion == 2:
        buscar_amino(amino_insert)
    if opcion == 3:
        eliminar_amino(amino, amino_insert)
    if opcion == 4:
        editar_amino(amino, amino_insert)
    if opcion == 5:
        print("Chao")
        print("PD: no salga de casa")
print("MENU")
amino_insert = {}
menu(amino, amino_insert)